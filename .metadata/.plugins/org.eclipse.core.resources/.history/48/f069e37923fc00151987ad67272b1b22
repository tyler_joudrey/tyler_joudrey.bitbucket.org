package www.Wumbo.gui;

import java.awt.Font;
import java.io.InputStream;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import www.Wumbo.data.Location;
import www.Wumbo.data.Course;
import www.Wumbo.data.Map;
import www.Wumbo.data.Question;
import www.Wumbo.data.Skills.SkillType;

public class Classroom extends BasicGameState {

	public static final int ID = 5;
	
	private StateBasedGame game;
	private GUIController controller;

	private TrueTypeFont font;
	private TrueTypeFont titleFont;

	Image background = null;
	private Image arrowUp = null;
	private Image arrowDown = null;
	private Image arrowLeft = null;
	private Image arrowRight = null;
	
	private int imageScale = 5;
	
	private boolean markDisplayed;
		
	private String class_bg_url = "art//Classroom.png";
	private InputProvider provider;
	
	private Question question;
	private Course course;
	
	public Classroom(GUIController controller) {
		this.controller = controller;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		this.game = game;
		
		background = new Image(class_bg_url, false, Image.FILTER_NEAREST);
		arrowUp = new Image("art//arrow-up.png", false, Image.FILTER_NEAREST);
		arrowDown = new Image("art//arrow-down.png", false, Image.FILTER_NEAREST);
		arrowLeft = new Image("art//arrow-left.png", false, Image.FILTER_NEAREST);
		arrowRight = new Image("art//arrow-right.png", false, Image.FILTER_NEAREST);
		
		provider = new InputProvider(gc.getInput());
		
		markDisplayed = false;
		
	    // load font from a .ttf file
	    try {
	        InputStream inputStream = ResourceLoader.getResourceAsStream("font//Thintel.ttf");
	         
	        Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
	        awtFont = awtFont.deriveFont(32f); // set font size
	        font = new TrueTypeFont(awtFont, false);
	        
	        Font awtFont2 = awtFont.deriveFont(56f); // set font size
	        titleFont = new TrueTypeFont(awtFont2, false);
	             
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		gc.setShowFPS(false);
		g.drawImage(background, 0, 0, 800, 450, 0, 0, 160, 90);
		
		Location room = controller.getModel().getMap().getLocation();
		
		course = controller.getModel().getCharacter().getBackpack().getCourse(room.getName());
		
		// Render the course name
		titleFont.drawString(180, 80, room.getName(), Color.white);
		
		if (course != null && course.getClassQuiz().hasNextQuestion()) {
			question = course.getClassQuiz().getQuestion(course.getClassQuiz().getNextQuestionIndex());
			
			String[] answers = question.getAnswers();
			
			// Render the question
			int valY = 100;
			valY = drawString(180, valY, question.getQuestion(), Color.white);
			
			// Render the answers
			int valX = 200;
			valY += 20;
			for (int i = 0; i < question.getNumAnswers(); i++) {
				String letter = "--";
				if (i == 0) {
					letter = "a) ";
				}
				if (i == 1) {
					letter = "b) ";
				}
				if (i == 2) {
					letter = "c) ";
				}
				if (i == 3) {
					letter = "d) ";
				}
				
				valY = drawString(valX, valY, letter + answers[i], Color.white);
				
				valY += 35 - font.getHeight();
			}
			
			int letterY = 150;
			titleFont.drawString(115, letterY, "A", Color.black);
			titleFont.drawString(115, letterY += titleFont.getHeight() + 10, "B", Color.black);
			titleFont.drawString(115, letterY += titleFont.getHeight() + 10, "C", Color.black);
			titleFont.drawString(115, letterY += titleFont.getHeight() + 10, "D", Color.black);

			
			
		} else {
			Map map = controller.getModel().getMap();
			
			//LEFT ARROW
			if (map.getWestLocation() != null) {
				g.drawImage(arrowLeft.getScaledCopy(imageScale), 20, 200);
			}
			
			//RIGHT ARROW
			if (map.getEastLocation() != null) {
				g.drawImage(arrowRight.getScaledCopy(imageScale), 740, 200);
			}
		}
		
		if (course != null && !course.getClassQuiz().hasNextQuestion() && !markDisplayed) {
			markDisplayed = true;
			course.getClassQuiz().takeTest();
			titleFont.drawString(300, 200, "You got " + course.getClassQuiz().calculateGrade() + "%!", Color.white);
			System.out.println(course.getGradeLetter());
			System.out.println(course.getClassQuiz().getGrade());
			System.out.println(course.getClassQuiz().calculateGrade());
		}
	}
	
	public int drawString(int x, int y, String text, Color color) {
	    for (String line : text.split("\n")) {
	        font.drawString(x, y += font.getHeight(), line, color);
	    }
	    return y;
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		if (button == 0) {
			System.out.println("X: " + x);
			System.out.println("Y: " + y);
			Map map = controller.getModel().getMap();
			
			//LEFT ARROW
			if ((x > 20 &&  x < 60) && (y > 203 &&  y < 238)) {
				if (map.getWestLocation() != null) {
					map.setLocation(map.getWestLocation());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//RIGHT ARROW
			if ((x > 741 &&  x < 781) && (y > 203 &&  y < 238)) {
				if (map.getEastLocation() != null) {
					map.setLocation(map.getEastLocation());	
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//ANSWER 1
			int letterY = 150;
			if ((x > 115 &&  x < 135) && (y > letterY &&  y < (letterY += titleFont.getHeight()))) {
				question.setChosenAnswerIndex(0);
				course.getClassQuiz().getNextQuestion();
				Display.update();
			}
			
			letterY += 10;
			//ANSWER 2
			if ((x > 115 &&  x < 135) && (y > letterY &&  y < (letterY += titleFont.getHeight()))) {
				question.setChosenAnswerIndex(1);
				course.getClassQuiz().getNextQuestion();
				Display.update();
			}
			
			letterY += 10;
			//ANSWER 3
			if ((x > 115 &&  x < 135) && (y > letterY &&  y < (letterY += titleFont.getHeight()))) {
				question.setChosenAnswerIndex(2);
				course.getClassQuiz().getNextQuestion();
				Display.update();
			}
			
			letterY += 10;
			//ANSWER 4
			if ((x > 115 &&  x < 135) && (y > letterY &&  y < (letterY += titleFont.getHeight()))) {
				question.setChosenAnswerIndex(3);
				course.getClassQuiz().getNextQuestion();
				Display.update();
			}
			

		}
	}
	
	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

}
