package www.Wumbo.gui;

import java.awt.Font;
import java.io.InputStream;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import www.Wumbo.enums.HairStyle;
import www.Wumbo.data.Character;
import www.Wumbo.data.Course;
import www.Wumbo.data.Skills.SkillType;

public class BackpackMenu extends BasicGameState {

	public static final int ID = 3;
	
	private StateBasedGame game;
	private GUIController controller;
	private Character character;
	
	private TrueTypeFont font;
	private TrueTypeFont titleFont;
	
	Image background = null;
	private int imageScale = 5;
	private int imageX = 15;
	private int imageY = 110;
	
	private String backpack_bg_url = "art//Backpack.png";
	private InputProvider provider;
	
	public BackpackMenu(GUIController controller) {
		this.controller = controller;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		this.game = game;
		
		background = new Image(backpack_bg_url, false, Image.FILTER_NEAREST);
		provider = new InputProvider(gc.getInput());
		
	    // load font from a .ttf file
	    try {
	        InputStream inputStream = ResourceLoader.getResourceAsStream("font//Thintel.ttf");
	         
	        Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
	        awtFont = awtFont.deriveFont(48f); // set font size
	        font = new TrueTypeFont(awtFont, false);
	           
	        Font awtFont2 = awtFont.deriveFont(56f); // set font size
	        titleFont = new TrueTypeFont(awtFont2, false);
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }  
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		gc.setShowFPS(false);
		g.drawImage(background, 0, 0, 800, 450, 0, 0, 160, 90);		
		
		character = controller.getModel().getCharacter();

		// render the character
		g.drawImage(new Image(character.getAppearance().getGender().getUrl(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		g.drawImage(new Image(character.getAppearance().getOutfit().getUrl(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		
		if (character.getAppearance().getHairStyle() != HairStyle.BALD) {
			g.drawImage(new Image(character.getAppearance().getHairStyle().getUrl() + character.getAppearance().getHairColour().getColour(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		}
		
		// Draw the Skills column
		titleFont.drawString(230, 100, "SKILLS", Color.white);
		
		int unusedPoints = character.getSkills().getUnusedPoints();
		
		if (unusedPoints > 0) {
			font.drawString(330, 100, "(" + String.valueOf(unusedPoints) + " Pts Left)", Color.white);
		}
		
		font.drawString(250, 150, "MATH", Color.white);
		font.drawString(400, 150, String.valueOf(character.getSkills().getMath()), Color.white);
		
		font.drawString(250, 200, "ARTS", Color.white);
		font.drawString(400, 200, String.valueOf(character.getSkills().getArts()), Color.white);
		
		font.drawString(250, 250, "LOGIC", Color.white);
		font.drawString(400, 250, String.valueOf(character.getSkills().getLogic()), Color.white);

		font.drawString(250, 300, "LISTENING", Color.white);
		font.drawString(400, 300, String.valueOf(character.getSkills().getListening()), Color.white);
		
		if (character.getSkills().hasUnusedPoints()) {
			font.drawString(450, 150, "+", Color.white);
			font.drawString(450, 200, "+", Color.white);
			font.drawString(450, 250, "+", Color.white);
			font.drawString(450, 300, "+", Color.white);

		}
		
		// Draw the Course column
		titleFont.drawString(550, 25, "COURSES", Color.white);
		
		Course[] courses = character.getBackpack().getCourses();
		
		int valY = 75;
		for (int i = 0; i < courses.length; i++) {
			int valX = 570;
			
			font.drawString(valX, valY, courses[i].getName(), Color.white);
			valX += 150;
			
			if (courses[i].hasTakenClass()) {
				font.drawString(valX, valY, courses[i].getGradeLetter(), Color.white);
			} else {
				font.drawString(valX, valY, "--", Color.white);
			}
			valY += 50;
		}

		
		// MENU, RETURN and EXIT buttons
		font.drawString(675, 410, "RETURN", Color.white);

		
	}
	
	@Override
	public void mouseReleased(int button, int x, int y) {
		if (button == 0) {
			System.out.println("X: " + x);
			System.out.println("Y: " + y);
			
			//MATH ADD
			if ((x > 450 &&  x < 466) && (y > 159 &&  y < 175)) {
				character.getSkills().usePoints(1, SkillType.MATH);
				Display.update();
			}
			
			//ARTS ADD
			if ((x > 450 &&  x < 466) && (y > 209 &&  y < 225)) {
				character.getSkills().usePoints(1, SkillType.ARTS);
				Display.update();
			}
			
			//LOGIC ADD
			if ((x > 450 &&  x < 466) && (y > 259 &&  y < 275)) {
				character.getSkills().usePoints(1, SkillType.LOGIC);
				Display.update();
			}
			
			//LISTENING ADD
			if ((x > 450 &&  x < 466) && (y > 309 &&  y < 325)) {
				character.getSkills().usePoints(1, SkillType.LISTENING);
				Display.update();
			}
			
			// RETURN BUTTON
			if ((x > 675 &&  x < 766) && (y > 415 &&  y < 437)) {
				game.enterState(controller.getModel().getMap().getLocation().getID());
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int arg2)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

}
