package www.Wumbo.gui;

import java.awt.Font;
import java.io.InputStream;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.command.BasicCommand;
import org.newdawn.slick.command.Command;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import www.Wumbo.data.Character;
import www.Wumbo.data.Appearance;
import www.Wumbo.data.GameModel;
import www.Wumbo.enums.Outfit;
import www.Wumbo.enums.Gender;
import www.Wumbo.enums.HairColour;
import www.Wumbo.enums.HairStyle;

public class CharacterCreation extends BasicGameState {

	public static final int ID = 2;
	
	private StateBasedGame game;
	private TrueTypeFont font, font2;
	private Font awtFont;
	private UnicodeFont uniFont;
	private TextField nameField;
	
	// The GUI Controller
	GUIController controller;
	
	// The text that is changeable, also drives images
	private Gender gender;
	private HairColour hairColour;
	private HairStyle hairStyle;
	private Outfit outfit;
	
	// Images
	private Image background = null;
	private int imageScale = 5;
	private int imageX = 15;
	private int imageY = 110;
	
	private String char_bg_url = "art//Character_Creation_Screen.png";
	private InputProvider provider;
	
	public CharacterCreation(GUIController controller) {
		this.controller = controller;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {
		this.game = game;
		this.gender = Gender.FEMALE;
		this.hairColour = HairColour.BLACK;
		this.hairStyle = HairStyle.LONG;
		this.outfit = Outfit.PINK_SHIRT;
		
		// load images
		background = new Image(char_bg_url, false, Image.FILTER_NEAREST);
		
		provider = new InputProvider(gc.getInput());

		// load a default java font
	    Font awtFont = new Font("Times New Roman", Font.BOLD, 24);
	    font = new TrueTypeFont(awtFont, false);
	    
	    uniFont = new UnicodeFont(awtFont);
	    uniFont.addGlyphs("@");
	    uniFont.getEffects().add(new ColorEffect(java.awt.Color.white));
	    nameField = new TextField(gc, uniFont, 50, 100, 75, 75);
	    nameField.setText("Name");
	    nameField.setBackgroundColor(Color.white);
	         
	    // load font from a .ttf file
	    try {
	        InputStream inputStream = ResourceLoader.getResourceAsStream("font//Thintel.ttf");
	         
	        Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
	        awtFont2 = awtFont2.deriveFont(48f); // set font size
	        font2 = new TrueTypeFont(awtFont2, false);
	             
	    } catch (Exception e) {
	        e.printStackTrace();
	    }  
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) 
			throws SlickException{
		gc.setShowFPS(false);
		g.drawImage(background, 0, 0, 800, 450, 0, 0, 160, 90);
		
		// render the textfield
		nameField = new TextField(gc, uniFont, 394, 78, 361, 38);
		nameField.render(gc, g);
		nameField.setText("Charlie");
		g.setFont(uniFont);
		
		
		// Draw the changeable strings
		font2.drawString(530, 148, gender.toString(), Color.black);
		font2.drawString(565, 213, hairColour.toString(), Color.black);
		font2.drawString(562, 280, hairStyle.toString(), Color.black);
		font2.drawString(445, 343, outfit.toString(), Color.black);

		
		
		// render the character
		g.drawImage(new Image(gender.getUrl(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		g.drawImage(new Image(outfit.getUrl(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		
		if (hairStyle != HairStyle.BALD) {
			g.drawImage(new Image(hairStyle.getUrl() + hairColour.getColour(), false, Image.FILTER_NEAREST).getScaledCopy(imageScale), imageX, imageY);
		}
	}
	
	/*
	private void printSimpleString(Graphics g, String s, int width, int XPos, int YPos){
        int stringLen = (int)
        g.getFontMetrics().getStringBounds(s, g).getWidth();
        int start = width/2 - stringLen/2;
        g.drawString(s, start + XPos, YPos);
	}*/

	@Override
	public void update(GameContainer gc, StateBasedGame game, int arg2) throws SlickException {
		//uniFont.loadGlyphs();
	}
	
	@Override
	public void mouseReleased(int button, int x, int y) {
		if (button == 0) {
			//System.out.println("X: " + x);
			//System.out.println("Y: " + y);
			
			// GENDER LEFT/RIGHT ARROW
			if (((x > 394 &&  x < 435) && (y > 149 &&  y < 184)) ||
					((x > 716 &&  x < 757) && (y > 149 &&  y < 184))) {
				if (gender.equals(Gender.MALE)) {
					gender = Gender.FEMALE;
				} else {
					gender = Gender.MALE;
				}
				Display.update();
			}
			
			// HAIR COLOUR LEFT ARROW
			if ((x > 465 &&  x < 506) && (y > 212 &&  y < 248)) {
				hairColour = hairColour.getPrevious();
				Display.update();
			}
			
			// HAIR COLOUR RIGHT ARROW
			if ((x > 716 &&  x < 757) && (y > 212 &&  y < 248)) {
				hairColour = hairColour.getNext();
				Display.update();
			}
			
			// HAIR STYLE LEFT ARROW
			if ((x > 436 &&  x < 476) && (y > 282 &&  y < 318)) {
				do {
					hairStyle = hairStyle.getPrevious();
				} while (!hairStyle.getGender().equals(gender));
				Display.update();
			}
		
			// HAIR STYLE RIGHT ARROW
			if ((x > 716 &&  x < 757) && (y > 277 &&  y < 312)) {
				do {
					hairStyle = hairStyle.getNext();
				} while (!hairStyle.getGender().equals(gender));
				Display.update();
			}
			
			// CLOTHING STYLE LEFT ARROW
			if ((x > 356 &&  x < 396) && (y > 342 &&  y < 378)) {
				do {
					outfit = outfit.getPrevious();
				} while (!outfit.getGender().equals(gender));
				Display.update();
			}
		
			// CLOTHING STYLE RIGHT ARROW
			if ((x > 641 &&  x < 683) && (y > 342 &&  y < 378)) {
				do {
					outfit = outfit.getNext();
				} while (!outfit.getGender().equals(gender));
				Display.update();
			}
			
			// GO! Button
			if ((x > 715 &&  x < 791) && (y > 368 &&  y < 443)) {
				
				Appearance appearance = new Appearance(gender, hairStyle, hairColour, outfit);
				Character character = new Character(nameField.getText(), appearance);
				controller.setModel(new GameModel(character));
				game.enterState(Hallway.ID);
			}
		}
	}

	@Override
	public int getID() {
		return ID;
	}
}
