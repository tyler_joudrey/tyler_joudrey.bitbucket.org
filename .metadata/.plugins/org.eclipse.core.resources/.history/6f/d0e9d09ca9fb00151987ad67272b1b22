package www.Wumbo.data;

import java.util.ArrayList;
import java.util.Random;

import www.Wumbo.gui.Bar;
import www.Wumbo.gui.Classroom;
import www.Wumbo.gui.Gym;
import www.Wumbo.gui.Hallway;
import www.Wumbo.gui.Library;
import www.Wumbo.gui.MealHall;
import www.Wumbo.gui.PuppyRoom;


/**
 * A location in Wumbo University. 
 * getcharacacter.getlocation()
 * @author: David Myers
 */

public class Map {
	private static final int MAX_Z = 2;
	private static final int MAX_Y = 3;
	private static final int MAX_X = 2;
	
	private static final Skills NO_BONUS = new Skills(0, 0, 0, 0);
	private static final Skills LIB_BONUS = new Skills(5, 5, 5, 5);
	private Location[][][] location;
	private Location current;
	private int xcord;
	private int ycord;
	private int zcord;
			
	public Map() {
		location = new Location[3][3][3];
		
		//Generating all the locations in the game
		Location MathClass = new Location("MathClass", Classroom.ID, "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location EconomicsClass = new Location("EconomicsClass", Classroom.ID, "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location CSClass = new Location("CSClass", Classroom.ID, "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location PhilosophyClass = new Location("PhilosophyClass", Classroom.ID, "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location ChemistryClass = new Location("ChemistryClass", Classroom.ID, "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location PuppyRoomRoom = new Location("Puppyroom", PuppyRoom.ID, "art//puppy-room.png", 1, 1, NO_BONUS, false, true);
		Location MealHallRoom = new Location("MealHall", MealHall.ID, "art//mealhall.png", 1, 1, NO_BONUS, false, false);
		Location GymRoom = new Location("Gym", Gym.ID, "art//Gym.png", 1, 1, NO_BONUS, false, false);
		Location BarRoom = new Location("Bar", Bar.ID, "art//bar.png", 1, 1, NO_BONUS, true, false);
		Location LibraryRoom = new Location("Library", Library.ID, "art//library.png", 0, 0, LIB_BONUS, false, false);
		//empty or abandoned classroom on map
		Location Empty = new Location("Empty");
		
		// Assigning non random locations to the map
		location[1][0][0] = new Location("Hallway Floor 1", Hallway.ID, "art//hall-start.png", 1, 1, NO_BONUS, false, false, new int[1][0][0]);
		location[1][1][0] = new Location("Hallway Floor 1", Hallway.ID, "art//hall-mid.png", 1, 1, NO_BONUS, false, false, new int[1][1][0]);
		location[1][2][0] = new Location("Hallway Floor 1", Hallway.ID, "art//hall-elevator.png", 1, 1, NO_BONUS, false, false, new int[1][2][0]);
		location[1][1][1] = new Location("Hallway Floor 2", Hallway.ID, "art//hall-mid.png", 1, 1, NO_BONUS, false, false, new int[1][1][1]);
		location[1][2][1] = new Location("Hallway Floor 2", Hallway.ID, "art//hall-elevator.png", 1, 1, NO_BONUS, false, false, new int[1][2][1]);
		location[1][1][2] = new Location("Hallway Floor 3", Hallway.ID, "art//hall-mid.png", 1, 1, NO_BONUS, false, false, new int[1][1][2]);
		location[1][2][2] = new Location("Hallway Floor 3", Hallway.ID, "art//hall-elevator.png", 1, 1, NO_BONUS, false, false, new int[1][2][2]);
		
		// Set starting location
		current = location[1][0][0];
		xcord = 1;
		ycord = 0;
		zcord = 0;
		
		// The list of locations that need to be randomized
		ArrayList<Location> randomLocation = new ArrayList<Location>();
		randomLocation.add(MathClass);
		randomLocation.add(EconomicsClass);
		randomLocation.add(CSClass);
		randomLocation.add(ChemistryClass);
		randomLocation.add(PhilosophyClass);
		randomLocation.add(PuppyRoomRoom);
		randomLocation.add(MealHallRoom);
		randomLocation.add(GymRoom);
		randomLocation.add(BarRoom);
		randomLocation.add(LibraryRoom);
		randomLocation.add(Empty);
		randomLocation.add(Empty);
		
		//Randomly adding 12 locations from randomLocation to our Map
		for (int i = 12; i < 1; i--){
			//Creating a random number between [0,arraylist)
			Random rn = new Random();
			int index = rn.nextInt(randomLocation.size());
			//randomly fills in the map and removes the location from randomLocation
			if (i == 12){
				location[0][1][0] = randomLocation.get(index);
				location[0][1][0].setCoords(new int[]{0, 1, 0});
				randomLocation.remove(index);
			}
			if (i == 11){
				location[0][2][0] = randomLocation.get(index);
				location[0][2][0].setCoords(new int[]{0, 2, 0});
				randomLocation.remove(index);
			}
			if (i == 10){
				location[2][1][0] = randomLocation.get(index);
				location[2][1][0].setCoords(new int[]{2, 1, 0});
				randomLocation.remove(index);
			}
			if (i == 9){
				location[2][2][0] = randomLocation.get(index);
				location[2][2][0].setCoords(new int[]{2, 2, 0});
				randomLocation.remove(index);
			}
			if (i == 8){
				location[0][1][1] = randomLocation.get(index);
				location[0][1][1].setCoords(new int[]{0, 1, 1});
				randomLocation.remove(index);
			}
			if (i == 7){
				location[0][2][1] = randomLocation.get(index);
				location[0][2][1].setCoords(new int[]{0, 2, 1});
				randomLocation.remove(index);
			}
			if (i == 6){
				location[2][1][1] = randomLocation.get(index);
				location[2][1][1].setCoords(new int[]{2, 1, 1});
				randomLocation.remove(index);
			}
			if (i == 5){
				location[2][2][1] = randomLocation.get(index);
				location[2][2][1].setCoords(new int[]{2, 2, 1});
				randomLocation.remove(index);
			}
			if (i == 4){
				location[0][1][2] = randomLocation.get(index);
				location[0][1][2].setCoords(new int[]{0, 1, 2});
				randomLocation.remove(index);
			}
			if (i == 3){
				location[0][2][2] = randomLocation.get(index);
				location[0][2][2].setCoords(new int[]{0, 2, 2});
				randomLocation.remove(index);
			}
			if (i == 2){
				location[2][1][2] = randomLocation.get(index);
				location[2][1][2].setCoords(new int[]{2, 1, 2});
				randomLocation.remove(index);
			}
			if (i == 1){
				location[2][2][2] = randomLocation.get(index);
				location[2][2][2].setCoords(new int[]{2, 2, 2});
				randomLocation.remove(index);
			}
		}
	}
	
	public int getFloor() {
		return zcord + 1;
	}
	
	public int getXCoordinate() {
		return xcord;
	}
	
	public int getYCoordinate() {
		return ycord;
	}
	
	public int getZCoordinate() {
		return zcord;
	}
	
	public void setLocation(Location room, int x, int y, int z) {
		location[x][y][z] = room;
	}
	
	public Location getLocation(int x, int y,int z) {
		Location current = location[x][y][z];
		return current;
	}
	
	public Location getLocation() {
		return location[xcord][ycord][ycord];
	}
	
	public boolean isLeftLocationEmpty() {
		if ( xcord - 1 < 0)
			return true;
		Location left = location[xcord - 1][ycord][ycord];
		if (left.equals("Empty"))
			return true;
		return false;
	}
	
	public boolean isRightLocationEmpty() {
		if (xcord + 1 > MAX_X)
			return true;
		Location right = location[xcord + 1][ycord][ycord];
		if (right.equals("Empty"))
			return true;
		return false;
	}
	
	public Location getWestLocation() {
		if (xcord - 1 < 0) {
			return null;
		} else {
			return location[xcord - 1][ycord][zcord];
		}	
	}
	
	public Location getEastLocation() {
		if (xcord + 1 > MAX_X) {
			return null;
		} else {
			return location[xcord + 1][ycord][zcord];
		}	
	}
	
	public Location getSouthLocation() {
		if (ycord - 1 < 0) {
			return null;
		} else {
			return location[xcord][ycord - 1][zcord];
		}	
	}
	
	public Location getNorthLocation() {
		if (ycord + 1 > MAX_Y) {
			return null;
		} else {
			return location[xcord][ycord + 1][zcord];
		}
	}
}