package www.Wumbo.data;

import java.util.ArrayList;
import java.util.Random;

/**
 * A location in Wumbo University. 
 * getcharacacter.getlocation()
 * @author: David Myers
 */

public class Map {
	private static final int MAX_Z = 2;
	private static final int MAX_Y = 3;
	private static final int MAX_X = 2;
	
	private static final Skills NO_BONUS = new Skills(0, 0, 0, 0);
	private static final Skills LIB_BONUS = new Skills(5, 5, 5, 5);
	private Location[][][] location;
	private Location current;
	private int xcord;
	private int ycord;
	private int zcord;
			
	public Map() {
		location = new Location[3][3][3];
		
		//Generating all the locations in the game
		Location MathClass = new Location("MathClass", "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location EconomicsClass = new Location("EconomicsClass", "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location CSClass = new Location("CSClass", "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location PhilosophyClass = new Location("PhilosophyClass", "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location ChemistryClass = new Location("ChemistryClass", "art//Classroom.png", 1, 1, NO_BONUS, false, false);
		Location PuppyRoom = new Location("Puppyroom","art//puppy-room.png", 1, 1, NO_BONUS, false, true);
		Location MealHall = new Location("MealHall", "art//mealhall.png", 1, 1, NO_BONUS, false, false);
		Location Gym = new Location("Gym", "art//Gym.png", 1, 1, NO_BONUS, false, false);
		Location Bar = new Location("Bar", "art//bar.png", 1, 1, NO_BONUS, true, false);
		Location Library = new Location("Library","art//library.png", 0, 0, LIB_BONUS, false, false);
		//empty or abandoned classroom on map
		Location Empty = new Location("Empty");
		
		// Assigning non random locations to the map
		location[1][1][0] = new Location("Hallway Floor 1", "art//hall-start.png", 1, 1, NO_BONUS, false, false);
		location[1][2][0] = new Location("Hallway Floor 1", "art//hall-mid.png", 1, 1, NO_BONUS, false, false);
		location[1][1][1] = new Location("Hallway Floor 2", "art//hall-start.png", 1, 1, NO_BONUS, false, false);
		location[1][2][1] = new Location("Hallway Floor 2", "art//hall-mid.png", 1, 1, NO_BONUS, false, false);
		location[1][1][2] = new Location("Hallway Floor 3", "art//hall-start.png", 1, 1, NO_BONUS, false, false);
		location[1][2][2] = new Location("Hallway Floor 3", "art//hall-mid.png", 1, 1, NO_BONUS, false, false);
		
		// The list of locations that need to be randomized
		ArrayList<Location> randomLocation = new ArrayList<Location>();
		randomLocation.add(MathClass);
		randomLocation.add(EconomicsClass);
		randomLocation.add(CSClass);
		randomLocation.add(ChemistryClass);
		randomLocation.add(PhilosophyClass);
		randomLocation.add(PuppyRoom);
		randomLocation.add(MealHall);
		randomLocation.add(Gym);
		randomLocation.add(Bar);
		randomLocation.add(Library);
		randomLocation.add(Empty);
		randomLocation.add(Empty);
		
		//Randomly adding 12 locations from randomLocation to our Map
		for (int i = 12; i < 1; i--){
			//Creating a random number between [0,arraylist)
			Random rn = new Random();
			int index = rn.nextInt(randomLocation.size());
			//randomly fills in the map and removes the location from randomLocation
			if (i == 12){
				location[0][1][0] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 11){
				location[0][2][0] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 10){
				location[2][1][0] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 9){
				location[2][2][0] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 8){
				location[0][1][1] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 7){
				location[0][2][1] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 6){
				location[2][1][1] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 5){
				location[2][2][1] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 4){
				location[0][1][2] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 3){
				location[0][2][2] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 2){
				location[2][1][2] = randomLocation.get(index);
				randomLocation.remove(index);
			}
			if (i == 1){
				location[2][2][2] = randomLocation.get(index);
				randomLocation.remove(index);
			}
		}
	}
	
	public int getFloor() {
		return zcord + 1;
	}
	
	public int getXCoordinate() {
		return xcord;
	}
	
	public int getYCoordinate() {
		return ycord;
	}
	
	public int getZCoordinate() {
		return zcord;
	}
	
	public void setLocation(Location room, int x, int y, int z) {
		location[x][y][z] = room;
	}
	
	public Location getLocation(int x, int y,int z) {
		Location current = location[x][y][z];
		return current;
	}
	
	public Location getLocation() {
		return location[xcord][ycord][ycord];
	}
	
	public String getLocationName() {
		Location name = location[xcord][ycord][ycord];
		return name.getName();
	}
	
	public boolean isLeftLocationEmpty() {
		if ( xcord - 1 < 0)
			return true;
		Location left = location[xcord - 1][ycord][ycord];
		if (left.equals("Empty"))
			return true;
		return false;
	}
	
	public boolean isRightLocationEmpty() {
		if (xcord + 1 > MAX_X)
			return true;
		Location right = location[xcord + 1][ycord][ycord];
		if (right.equals("Empty"))
			return true;
		return false;
	}
	
	public Location getWestLocation() {
		if (xcord - 1 < 0) {
			return null;
		} else {
			return location[xcord][ycord - 1][zcord];
		}	
	}
	
	public Location getEastLocation() {
		if (xcord + 1 > MAX_X) {
			return null;
		} else {
			return location[xcord][ycord - 1][zcord];
		}	
	}
	
	public Location getNorthLocation() {
		if (ycord - 1 < 0) {
			return null;
		} else {
			return location[xcord][ycord - 1][zcord];
		}	
	}
	
	public Location getSouthLocation() {
		if (ycord + 1 > MAX_Y) {
			return null;
		} else {
			return location[xcord][ycord - 1][zcord];
		}
	}
}