package www.Wumbo.data;

import www.Wumbo.gui.Classroom;

/**
 * The character's backpack which holds all of their courses and course marks.
 * It also includes all of their tests and exams and assignments.
 * 
 * @author: Luke Webster
 */
public class Backpack {

	private final int NUM_COURSES = 5;
	private final double MAX_GPA = 4.0;
	private final int MIN_GPA = 0;

	private Course[] courses;
	private double gpa;

	public Backpack() {
		this.courses = new Course[5];
		
		Question[] questionsQuizMath = new Question[] {
				new Question("Which of the following is a subset of  {b, c, d}?", 
						new String[]{"{ }", "{a}", "{1 , 2 , 3}", "{a, b, c}"}, 0),
				new Question("The value of 5 in the number  357.21 is", 
						new String[]{"5 tenths", "5 ones", "5 tens", "5 hundreds"}, 2),
				new Question("In coordinate geometry, the equation of the x-axis is", 
						new String[]{"y = x", "y = 0", "x = 0", "y = 1"}, 1),
				new Question("3x - 4(x + 6) =", 
						new String[]{"x + 6", "-x - 24", "7x + 6", " -7x - 24"}, 1),
				new Question("3 and 4/5 expressed as a decimal is", 
						new String[]{"3.40", "3.45", "3.50", "3.80"}, 3)
			};
		Test classQuizMath = new Test(questionsQuizMath, MIN_GPA);
		
		Question[] questionAssMath = new Question[]{new Question("Which of the following is the Highest Common Factor of 18, 24 and 36?", 
				new String[]{"6", "18", "36", "72"}, 0)};
		Test assignmentMath = new Test(questionAssMath, MIN_GPA);
		
		this.courses[0] = new Course("MATH 101", classQuizMath, assignmentMath, null, MIN_GPA);

		
		Question[] questionsQuizEcon = new Question[] {
				new Question("Economics is the study of:", 
						new String[]{"production technology", "consumption decisions", "how society decides what, how, and for whom to produce", "the best way to run society"}, 2),
				new Question("The opportunity cost of a good is:", 
						new String[]{"the time lost in finding it", "the quantity of other goods sacrificed to get another unit of that good", "the expenditure on the good", "the loss of interest in using savings"}, 1),
				new Question("A market can accurately be described as:", 
						new String[]{"a place to buy things", "a place to sell things", "the process by which prices adjust to reconcile the allocation of resources", "a place where buyers and sellers meet"}, 1),
				new Question("In a free market __________", 
						new String[]{"governments intervene", "governments plan production", "governments interfere", "prices adjust to reconcile scarcity and desires"}, 3),
				new Question("In the mixed economy", 
						new String[]{"economic problems are solved by the government and market", "economic decisions are made by the private sector and free market", "economic allocation is achieved by the invisible hand", "economic questions are solved by government department"}, 0)
			};
		Test classQuizEcon = new Test(questionsQuizMath, MIN_GPA);
		
		Question[] questionAssEcon = new Question[]{new Question("Microeconomics is concerned with", 
				new String[]{"the economy as a whole", "the electronics industry", "the study of individual economic behaviour", "the interactions within the entire economy"}, 2)};
		Test assignmentEcon = new Test(questionAssMath, MIN_GPA);
		
		this.courses[1] = new Course("ECON 101", classQuizEcon, assignmentEcon, null, MIN_GPA);
		
		
		Question[] questionsQuizComp = new Question[] {
				new Question("What is the numerical range of a char in Java?", 
						new String[]{"-128 to 127", "0 to 256", "0 to 32767", "0 to 65535"}, 3),
				new Question("Which of these coding types is used for data type characters in Java?", 
						new String[]{"ASCII", "ISO-LATIN-1", "UNICODE", "None of the above"}, 2),
				new Question("Which of these values can a boolean variable contain?", 
						new String[]{"True & False", "0 & 1", "Any integer value", "true"}, 0),
				new Question("Which of these occupy first 0 to 127 in Unicode character set used for characters in Java?", 
						new String[]{"ASCII", "ISO-LATIN-1", "ASCII and ISO-LATIN1", "None of the above"}, 2),
				new Question("Which one is a valid declaration of a boolean?", 
						new String[]{"boolean b1 = 1;", "boolean b2 = �false�;", "boolean b3 = false;", "boolean b4 = �true�"}, 2)
			};
		Test classQuizComp = new Test(questionsQuizMath, MIN_GPA);
		
		Question[] questionAssComp = new Question[]{new Question("Which of these operators is used to allocate memory to array variable in Java?", 
				new String[]{"malloc", "alloc", "new", "new malloc"}, 2)};
		Test assignmentComp = new Test(questionAssMath, MIN_GPA);
		
		this.courses[2] = new Course("COMP 101", classQuizComp, assignmentComp, null, MIN_GPA);
		
		Question[] questionsQuizPhil = new Question[] {
				new Question("Who is a philosopher, in the original sense of the world?", 
						new String[]{"Someone who studies the stars and planets.", "A person primarily interested in the truth about moral matters.", "A lover and pursuer of wisdom, regardless of the subject matter.", "A clever and tricky arguer."}, 2),
				new Question("Which is a common characteristic of philosophical questions?", 
						new String[]{"They are strictly empirical questions.", "They involve fundamental concepts that are unavoidable by the thoughtful person.", "They are purely semantic questions.", "They aren't relevant to ordinary, everyday situations."}, 1),
				new Question("Which is the branch of philosophy that studies issues concerning art and beauty?", 
						new String[]{"Aesthetics", "Epistemology", "Logic", "Metaphysics"}, 0),
				new Question("Which of the following branches of philosophy does not involve questions related to values?", 
						new String[]{"Moral", "Metaphysics", "Social", "Political"}, 1),
				new Question("In philosophy, what is an argument?", 
						new String[]{"A factual disagreement between people.", "Giving reasons for a belief.", "A shouting match.", "Any verbal attempt to persuade"}, 1)
			};
		Test classQuizPhil = new Test(questionsQuizMath, MIN_GPA);
		
		Question[] questionAssPhil = new Question[]{new Question("What fallacy is it when an argument attacks the person rather than the person's beliefs?", 
				new String[]{"Red Herring", "Begging the Question", "Straw Man", "Argumentum ad Hominem"}, 3)};
		Test assignmentPhil = new Test(questionAssMath, MIN_GPA);
		
		this.courses[3] = new Course("PHIL 101", classQuizPhil, assignmentPhil, null, MIN_GPA);
		
		
		Question[] questionsQuizChem = new Question[] {
				new Question("Which of the following has a positive charge?", 
						new String[]{"proton", "neutron", "anion", "electron"}, 0),
				new Question("The maximum number of electrons that can be accommodated in a sublevel for which l = 3 is:", 
						new String[]{"2", "10", "6", "14"}, 3),
				new Question("", 
						new String[]{"", "", "", ""}, 1),
				new Question("", 
						new String[]{"", "", "", ""}, 1),
				new Question("", 
						new String[]{"", "", "", ""}, 3)
			};
		Test classQuizChem = new Test(questionsQuizMath, MIN_GPA);
		
		Question[] questionAssChem = new Question[]{new Question("", 
				new String[]{"", "", "", ""}, 0)};
		Test assignmentChem = new Test(questionAssMath, MIN_GPA);
		
		this.courses[4] = new Course("CHEM 101", classQuizChem, assignmentChem, null, MIN_GPA);
		
		
	}

	public double calculateGpa() {
		double newGpa = 0;
		
		for(int i = 0; i < NUM_COURSES; i++) {
			newGpa += courses[i].getGrade();
		}
		
		newGpa = newGpa / NUM_COURSES;
		
		return newGpa;
	}
	
	// Getters and setters beyond this point
	public Course getCourse(String name) {
		Course course = null;
		
		for (int i = 0; i < courses.length; i++) {
			if (name == courses[i].getName()) {
				course = courses[i];
			}
		}	
		return course;
	}
	
	public Course[] getCourses() {
		return courses;
	}

	public void setCourses(Course[] courses) {
		this.courses = courses;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}
}
