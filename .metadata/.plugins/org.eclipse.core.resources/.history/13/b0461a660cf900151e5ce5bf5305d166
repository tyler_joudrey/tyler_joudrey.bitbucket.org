package www.Wumbo.data;

public class Character {

	private final int MAX_LEVEL = 20;
	private final int MAX_EXPERIENCE = 100;
	private final int MAX_HEALTH = 200;
	private final int INCREMENT_HEALTH = 10;
	private final int DECREMENT_HEALTH = 10;
	private final int DECREMENT_HEALTH_GYM = 30;
	private final int MAX_SANITY = 100;
	private final int LEVEL_UP_POINTS = 5;
	private final double MAX_LUCK = 1.0;

	
	private String name;
	private int health;
	private int maxHealth;
	private int sanity;
	private double luck;
	private int level;
	private int experience;
	private double accountBalance;
	private Skills skills;
	private Appearance appearance;
	private Backpack backpack;
	private Location location;
	
	
	public Character(String name, int health, int maxHealth, int sanity,
			float luck, int level, int experience, double accountBalance, 
			Skills skills, Appearance appearance, Backpack backpack, 
			Location location) {
		super();
		this.name = name;
		this.health = health;
		this.maxHealth = maxHealth;
		this.sanity = sanity;
		this.luck = luck;
		this.level = level;
		this.experience = experience;
		this.accountBalance = accountBalance;
		this.skills = skills;
		this.appearance = appearance;
		this.backpack = backpack;
		this.location = location;
	}

	public void gainExperience(int experience) {
		if ((this.experience + experience) > MAX_EXPERIENCE) {
			levelUp();
			this.experience = this.experience + experience - MAX_EXPERIENCE;
		}
		else {
			this.experience += experience; 
		}
	}
	
	public void levelUp() {
		if (level < MAX_LEVEL) {
			level++;
			skills.addUnusedPoints(LEVEL_UP_POINTS);
		}
		else {
			System.out.println("Max Level reached! Can't level up.");
		}
	}
	
	public void useGymEquipment() {
		if (maxHealth < MAX_HEALTH) {
			maxHealth += INCREMENT_HEALTH;
		}
		
		// Additional health lost from using gym equipment
		health -= DECREMENT_HEALTH_GYM;
	}
	
	public void replenishHealth() {
		health = maxHealth;
	}
	
	public void replenishSanity() {
		sanity = MAX_SANITY;
	}
	
	public void replenishLuck() {
		luck = MAX_LUCK;
	}
	
	// Getters and Setters beyond this point
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getHealth() {
		return health;
	}


	public void setHealth(int health) {
		this.health = health;
	}


	public int getMaxHealth() {
		return maxHealth;
	}


	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}


	public int getSanity() {
		return sanity;
	}


	public void setSanity(int sanity) {
		this.sanity = sanity;
	}


	public double getLuck() {
		return luck;
	}


	public void setLuck(double luck) {
		this.luck = luck;
	}


	public double getGpa() {
		return gpa;
	}


	public void setGpa(double gpa) {
		this.gpa = gpa;
	}


	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public int getExperience() {
		return experience;
	}


	public void setExperience(int experience) {
		this.experience = experience;
	}


	public double getAccountBalance() {
		return accountBalance;
	}


	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}


	public Skills getSkills() {
		return skills;
	}


	public void setSkills(Skills skills) {
		this.skills = skills;
	}


	public Appearance getAppearance() {
		return appearance;
	}


	public void setAppearance(Appearance appearance) {
		this.appearance = appearance;
	}


	public Backpack getBackpack() {
		return backpack;
	}


	public void setBackpack(Backpack backpack) {
		this.backpack = backpack;
	}


	public Location getLocation() {
		return location;
	}


	public void setLocation(Location location) {
		this.location = location;
	}
	
}
