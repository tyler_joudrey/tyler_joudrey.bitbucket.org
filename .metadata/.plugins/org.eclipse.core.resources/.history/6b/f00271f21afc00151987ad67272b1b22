package www.Wumbo.data;

/**
 * A course object that contains all tests in that course and 
 * the grade that the student has.
 * 
 * @author: Luke Webster
 */
public class Course {

	private String name;
	private Test classQuiz;
	private Test assignment;
	private Test exam;
	private double grade;
	private boolean takenClass;
	
	
	public Course(String name, Test classQuiz, Test assignment, Test exam) {
		super();
		this.name = name;
		this.classQuiz = classQuiz;
		this.assignment = assignment;
		this.exam = exam;
		this.grade = 0;
		this.takenClass = false;
	}

	public boolean hasTakenClass() {
		return takenClass;
	}
	
	public void takeClass() {
		takenClass = true;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Test getClassQuiz() {
		return classQuiz;
	}


	public void setClassQuiz(Test classQuiz) {
		this.classQuiz = classQuiz;
	}


	public Test getAssignment() {
		return assignment;
	}


	public void setAssignment(Test assignment) {
		this.assignment = assignment;
	}


	public Test getExam() {
		return exam;
	}


	public void setExam(Test exam) {
		this.exam = exam;
	}


	public String getGradeLetter() {
		if (grade > 0.94) {
			return "A+";
		}
		if (grade > 0.87) {
			return "A";
		}
		if (grade > 0.80) {
			return "A-";
		}
		if (grade > 0.77) {
			return "B+";
		}
		if (grade > 0.73) {
			return "B";
		}
		if (grade > 0.70) {
			return "B-";
		}
		if (grade > 0.67) {
			return "C+";
		}
		if (grade > 0.63) {
			return "C";
		}
		if (grade > 0.60) {
			return "C-";
		}
		if (grade > 0.57) {
			return "D+";
		}
		if (grade > 0.53) {
			return "D";
		}
		if (grade > 0.50) {
			return "D-";
		} else {
			return "F";
		}
	}
	
	public double getGrade() {
		return grade;
	}


	public void setGrade(double grade) {
		this.grade = grade;
	}
}
