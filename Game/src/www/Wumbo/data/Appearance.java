package www.Wumbo.data;

import www.Wumbo.enums.*;

/**
 * The character's saved appearance for use when drawing them in the game.
 * 
 * @author: Luke Webster
 */
public class Appearance {

	private Gender gender;
	private HairStyle hairStyle;
	private HairColour hairColour;
	private Outfit outfit;
	
	public Appearance(Gender gender, HairStyle hairStyle,
			HairColour hairColour, Outfit outfit) {
		super();
		this.gender = gender;
		this.hairStyle = hairStyle;
		this.hairColour = hairColour;
		this.outfit = outfit;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public HairStyle getHairStyle() {
		return hairStyle;
	}

	public void setHairStyle(HairStyle hairStyle) {
		this.hairStyle = hairStyle;
	}

	public HairColour getHairColour() {
		return hairColour;
	}

	public void setHairColour(HairColour hairColour) {
		this.hairColour = hairColour;
	}

	public Outfit getOutfit() {
		return outfit;
	}

	public void setOutfit(Outfit outfit) {
		this.outfit = outfit;
	}
}
