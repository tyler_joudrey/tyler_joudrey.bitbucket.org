package www.Wumbo.data;

import www.Wumbo.gui.Classroom;

/**
 * A location in Wumbo University. 
 * 
 * @author: Luke Webster
 */
public class Location {

	private final int ID;
	
	private String name;
	private int noise;
	private int distractions;
	private Skills skillBonus;
	private boolean replenishLuck;
	private boolean replenishSanity;
	private int[] coords;
	private String imageUrl;
	
	public Location(String name, int id, String imageUrl, int noise, int distractions,
			Skills skillBonus, boolean replenishLuck, boolean replenishSanity) {
		super();
		this.name = name;
		this.ID = id;
		this.imageUrl = imageUrl;
		this.noise = noise;
		this.distractions = distractions;
		this.skillBonus = skillBonus;
		this.replenishLuck = replenishLuck;
		this.replenishSanity = replenishSanity;
	}
	
	public Location(String name, int id, String imageUrl, int noise, int distractions,
			Skills skillBonus, boolean replenishLuck, boolean replenishSanity, int[] coords) {
		super();
		this.name = name;
		this.ID = id;
		this.imageUrl = imageUrl;
		this.noise = noise;
		this.distractions = distractions;
		this.skillBonus = skillBonus;
		this.replenishLuck = replenishLuck;
		this.replenishSanity = replenishSanity;
		this.coords = coords;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoise() {
		return noise;
	}

	public void setNoise(int noise) {
		this.noise = noise;
	}

	public int getDistractions() {
		return distractions;
	}

	public void setDistractions(int distractions) {
		this.distractions = distractions;
	}

	public int[] getCoords() {
		return coords;
	}
	
	public int getCoordX() {
		return coords[0];
	}
	
	public int getCoordY() {
		return coords[1];
	}
	
	public int getCoordZ() {
		return coords[2];
	}

	public void setCoords(int[] coords) {
		this.coords = coords;
	}

	public Skills getSkillBonus() {
		return skillBonus;
	}


	public void setSkillBonus(Skills skillBonus) {
		this.skillBonus = skillBonus;
	}


	public boolean isReplenishLuck() {
		return replenishLuck;
	}


	public void setReplenishLuck(boolean replenishLuck) {
		this.replenishLuck = replenishLuck;
	}


	public boolean isReplenishSanity() {
		return replenishSanity;
	}


	public void setReplenishSanity(boolean replenishSanity) {
		this.replenishSanity = replenishSanity;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getID() {
		return ID;
	}
}
