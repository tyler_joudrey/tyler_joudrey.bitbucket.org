package www.Wumbo.data;

/**
 * A non-player character such as a professor. Has traits which affect
 * their teaching ability.
 * 
 * @author: Luke Webster
 */
public class NonPlayerCharacter {

	private String name;
	private Appearance appearance;
	private int professorTrait;
	
	public NonPlayerCharacter(String name, Appearance appearance,
			int professorTrait) {
		super();
		this.name = name;
		this.appearance = appearance;
		this.professorTrait = professorTrait;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Appearance getAppearance() {
		return appearance;
	}

	public void setAppearance(Appearance appearance) {
		this.appearance = appearance;
	}

	public int getProfessorTrait() {
		return professorTrait;
	}

	public void setProfessorTrait(int professorTrait) {
		this.professorTrait = professorTrait;
	}
}
