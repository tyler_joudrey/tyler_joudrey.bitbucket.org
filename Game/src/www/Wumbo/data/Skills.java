package www.Wumbo.data;

/**
 * The character's skills. 
 * 
 * @author: Luke Webster
 */
public class Skills {

	public enum SkillType{MATH, ARTS, LOGIC, LISTENING};
	
	private final int MAX_SKILL_LEVEL = 20;
	private final int INIT_MATH = 5;
	private final int INIT_ARTS = 5;
	private final int INIT_LOGIC = 5;
	private final int INIT_LISTENING = 5;
	private final int INIT_UNUSED_POINTS = 10;
	
	private int math;
	private int arts;
	private int logic;
	private int listening;
	private int unusedPoints;


	public Skills() {
		super();
		this.math = INIT_MATH;
		this.arts = INIT_ARTS;
		this.logic = INIT_LOGIC;
		this.listening = INIT_LISTENING;
		unusedPoints = INIT_UNUSED_POINTS;
	}
	
	public Skills(int math, int arts, int logic, int listening) {
		super();
		this.math = math;
		this.arts = arts;
		this.logic = logic;
		this.listening = listening;
		this.unusedPoints = 0;
	}

	public boolean hasUnusedPoints() {
	return unusedPoints > 0;	
		
	}
	
	public void addUnusedPoints(int amount) {
		unusedPoints += amount;
	}
	
	public void usePoints(int amount, SkillType skill) {
		switch(skill) {
		case MATH:
			if (math + amount <= MAX_SKILL_LEVEL) {
				math += amount;
			}
			break;
		case ARTS:
			if (arts + amount <= MAX_SKILL_LEVEL) {
				arts += amount;
			}
			break;
		case LOGIC:
			if (logic + amount <= MAX_SKILL_LEVEL) {	
				logic += amount;
			}
			break;
		case LISTENING:
			if (listening + amount <= MAX_SKILL_LEVEL) {
				listening += amount;
			}
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		unusedPoints--;
	}
	
	public int getMath() {
		return math;
	}

	public void setMath(int math) {
		this.math = math;
	}

	public int getArts() {
		return arts;
	}

	public void setArts(int arts) {
		this.arts = arts;
	}

	public int getLogic() {
		return logic;
	}

	public void setLogic(int logic) {
		this.logic = logic;
	}

	public int getListening() {
		return listening;
	}

	public void setListening(int listening) {
		this.listening = listening;
	}
	
	public int getUnusedPoints() {
		return unusedPoints;
	}

	public void setUnusedPoints(int unusedPoints) {
		this.unusedPoints = unusedPoints;
	}
	
	
}
