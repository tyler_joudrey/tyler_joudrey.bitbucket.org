package www.Wumbo.data;

/**
 * A course object that contains all tests in that course and 
 * the grade that the student has.
 * 
 * @author: Luke Webster
 */
public class Course {

	private static final double QUIZ_WEIGHT = 0.9;
	private static final double ASSIGNMENT_WEIGHT = 0.1;
	private static final double EXAM_WEIGHT = 0.5;

	
	private String name;
	private Test classQuiz;
	private Test assignment;
	private Test exam;
	private double grade;
	
	
	public Course(String name, Test classQuiz, Test assignment, Test exam) {
		super();
		this.name = name;
		this.classQuiz = classQuiz;
		this.assignment = assignment;
		this.exam = exam;
		this.grade = 0;
	}
	
	public double calculateGrade() {
		int tempGrade = 0;
		int numTestsTaken = 0;
		if (classQuiz.hasTakenTest()) {
			tempGrade += classQuiz.calculateGrade() * QUIZ_WEIGHT;
			numTestsTaken++;
		}
		if (assignment.hasTakenTest()) {
			tempGrade += assignment.calculateGrade() * ASSIGNMENT_WEIGHT;
			numTestsTaken++;
		}
		if (exam != null && exam.hasTakenTest()) {
			tempGrade += exam.calculateGrade() * EXAM_WEIGHT;
			numTestsTaken++;
		}
		
		grade = tempGrade / numTestsTaken;
		
		return grade;
	}

	public boolean hasTakenClass() {
		boolean takenClass = false;
		if (classQuiz.hasTakenTest() || assignment.hasTakenTest()/* || exam.hasTakenTest()*/) {
			takenClass = true;
		}
		
		return takenClass;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Test getClassQuiz() {
		return classQuiz;
	}


	public void setClassQuiz(Test classQuiz) {
		this.classQuiz = classQuiz;
	}


	public Test getAssignment() {
		return assignment;
	}


	public void setAssignment(Test assignment) {
		this.assignment = assignment;
	}


	public Test getExam() {
		return exam;
	}


	public void setExam(Test exam) {
		this.exam = exam;
	}


	public String getGradeLetter() {		
		if (grade > 0.94) {
			return "A+";
		}
		else if (grade > 0.87) {
			return "A";
		}
		else if (grade > 0.80) {
			return "A-";
		}
		else if (grade > 0.77) {
			return "B+";
		}
		else if (grade > 0.73) {
			return "B";
		}
		else if (grade > 0.70) {
			return "B-";
		}
		else if (grade > 0.67) {
			return "C+";
		}
		else if (grade > 0.63) {
			return "C";
		}
		else if (grade > 0.60) {
			return "C-";
		}
		else if (grade > 0.57) {
			return "D+";
		}
		else if (grade > 0.53) {
			return "D";
		}
		else if (grade > 0.50) {
			return "D-";
		} else {
			return "F";
		}
	}
	
	public double getGrade() {
		return grade;
	}


	public void setGrade(double grade) {
		this.grade = grade;
	}
}
