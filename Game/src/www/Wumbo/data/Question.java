package www.Wumbo.data;

import java.sql.Time;

/**
 * One question that can be used on Test objects. Includes all possible
 * answers, the correct answer, and the answer that they chose once they choose it.
 * 
 * @author: Luke Webster
 */
public class Question {

	private String question;
	private String[] answers;
	private int numAnswers;
	private int correctAnswerIndex;
	private int chosenAnswerIndex;
	private Time timeRemaining;
	
	
	public Question(String question, String[] answers, int correctAnswerIndex) {
		super();
		this.question = question;
		this.answers = answers;
		this.numAnswers = 4;
		this.correctAnswerIndex = correctAnswerIndex;
	}

	public boolean isAnswerCorrect() {
		return correctAnswerIndex == chosenAnswerIndex ? true : false;
	}
	
	public void startQuestion() {
		//do time stuff here
	}
	
	// Getters and setters beyond this point
	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String[] getAnswers() {
		return answers;
	}


	public void setAnswers(String[] answers) {
		this.answers = answers;
	}


	public int getNumAnswers() {
		return numAnswers;
	}


	public void setNumAnswers(int numAnswers) {
		this.numAnswers = numAnswers;
	}


	public int getCorrectAnswerIndex() {
		return correctAnswerIndex;
	}


	public void setCorrectAnswerIndex(int correctAnswerIndex) {
		this.correctAnswerIndex = correctAnswerIndex;
	}


	public int getChosenAnswerIndex() {
		return chosenAnswerIndex;
	}


	public void setChosenAnswerIndex(int chosenAnswerIndex) {
		this.chosenAnswerIndex = chosenAnswerIndex;
	}


	public Time getTimeRemaining() {
		return timeRemaining;
	}


	public void setTimeRemaining(Time timeRemaining) {
		this.timeRemaining = timeRemaining;
	}
}
