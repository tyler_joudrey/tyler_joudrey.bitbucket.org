package www.Wumbo.data;

/**
 * An abstract class that can be used for quizzes, exams, and assignments. 
 * Hold an array of questions and a grade.
 * 
 * @author: Luke Webster
 */
public class Test {

	private Question[] questions;
	private int numQuestions;
	private double grade;
	private int qIndex;
	private boolean takenTest;
	
	
	public Test(Question[] questions) {
		super();
		this.questions = questions;
		this.grade = 0;
		qIndex = 0;
		this.takenTest = false;
		
		numQuestions = questions.length;
	}
	
	public boolean hasTakenTest() {
		return takenTest;
	}
	
	public void takeTest() {
		takenTest = true;
	}
	
	public double calculateGrade() {
		for (int i = 0; i < questions.length; i++) {
			if (questions[i].isAnswerCorrect()) {
				grade++;
			}
		}
		grade = grade / questions.length;
		
		return grade;
	}
	
	public boolean hasNextQuestion() {
		boolean nextQuestion = false;
		if (qIndex < questions.length) {
			nextQuestion = true;
		}
		
		return nextQuestion;
	}
	
	public Question getNextQuestion() {
		Question nextQuestion = null;
		if (qIndex < questions.length) {
			nextQuestion = questions[qIndex];
			qIndex++;
		}
		
		return nextQuestion;
	}
	
	public int getNextQuestionIndex() {
		return qIndex;
	}

	public Question[] getQuestions() {
		return questions;
	}


	public void setQuestions(Question[] questions) {
		this.questions = questions;
	}


	public double getGrade() {
		return grade;
	}


	public void setGrade(double grade) {
		this.grade = grade;
	}
	
	
	public Question getQuestion(int index) {
		return questions[index];
	}
	
	
	public void setQuestion(Question question, int index) {
		questions[index] = question;
	}


	public int getNumQuestions() {
		return numQuestions;
	}


	public void setNumQuestions(int numQuestions) {
		this.numQuestions = numQuestions;
	}
}
