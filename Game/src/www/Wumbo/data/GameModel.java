package www.Wumbo.data;

public class GameModel {

	private Character character;
	private Map map;
	
	public GameModel(Character character) {
		super();
		this.character = character;
		this.map = new Map();
	}

	public Character getCharacter() {
		return character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}
	
	
	
}
