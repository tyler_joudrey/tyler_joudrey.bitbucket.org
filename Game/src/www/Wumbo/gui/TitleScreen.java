package www.Wumbo.gui;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.command.BasicCommand;
import org.newdawn.slick.command.Command;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.CrossStateTransition;
import org.newdawn.slick.state.transition.EmptyTransition;
/**
 * Wumbo Title Screen
 * 
 * @author Tyler B. Joudrey & Luke Webster
 *
 */
public class TitleScreen extends BasicGameState {

	public static final int ID = 0;
	
	private StateBasedGame game;
	
	Image img = null;
	private String title_url = "art//main_screen_1.png";
	private InputProvider provider;
	private boolean enter;
	public static final int CREATION_SCREEN = CharacterCreation.ID;	

	public TitleScreen(){}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
	throws SlickException {
		this.game = game;
		
		img = new Image(title_url, false, Image.FILTER_NEAREST);
		provider = new InputProvider(gc.getInput());
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int i) 
	throws SlickException {
	}
	
	public void keyReleased(int key, char c) {
	      if (key == Input.KEY_ENTER) {
	         // how to change states
	    	  GameState target = game.getState(CharacterCreation.ID);
	         
	         final long start = System.currentTimeMillis();
	         CrossStateTransition t = new CrossStateTransition(target) {            
	            public boolean isComplete() {
	               return (System.currentTimeMillis() - start) > 2000;
	            }

	            public void init(GameState firstState, GameState secondState) {
	            }
	         };
	         
	         
	         game.enterState(CharacterCreation.ID, t, new EmptyTransition());
	      }
	   }

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) 
			throws SlickException{
		gc.setShowFPS(false);
		g.drawImage(img, 0, 0, 800, 450, 0, 0, 160, 90);
	}

	@Override
	public int getID() {
		return ID;
	}
	
}
