package www.Wumbo.gui;

import java.awt.Font;
import java.io.InputStream;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.ResourceLoader;

import www.Wumbo.data.Map;

public class Hallway extends BasicGameState {

	public static final int ID = 4;
	
	public static final String BACKPACK_BUTTON = "BACKPACK";
	
	private StateBasedGame game;
	private TrueTypeFont font;
	private TrueTypeFont titleFont;
	private GUIController controller;

	private Image background = null;
	private Image arrowUp = null;
	private Image arrowDown = null;
	private Image arrowLeft = null;
	private Image arrowRight = null;
	
	private int imageScale = 5;
	
	private String hall_start_url = "art//hall-start.png";
	private InputProvider provider;
	
	public Hallway(GUIController controller) {
		this.controller = controller;

	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		this.game = game;
		
		background = new Image(hall_start_url, false, Image.FILTER_NEAREST);
		
		arrowUp = new Image("art//arrow-up.png", false, Image.FILTER_NEAREST);
		arrowDown = new Image("art//arrow-down.png", false, Image.FILTER_NEAREST);
		arrowLeft = new Image("art//arrow-left.png", false, Image.FILTER_NEAREST);
		arrowRight = new Image("art//arrow-right.png", false, Image.FILTER_NEAREST);
		
		
		provider = new InputProvider(gc.getInput());
		
	    // load font from a .ttf file
	    try {
	        InputStream inputStream = ResourceLoader.getResourceAsStream("font//Thintel.ttf");
	         
	        Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
	        awtFont = awtFont.deriveFont(48f); // set font size
	        font = new TrueTypeFont(awtFont, false);
	        
	        Font awtFont2 = awtFont.deriveFont(56f); // set font size
	        titleFont = new TrueTypeFont(awtFont2, false);
	             
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		gc.setShowFPS(false);
		Map map = controller.getModel().getMap();
		
		background = new Image(map.getLocation().getImageUrl(), false, Image.FILTER_NEAREST);
		g.drawImage(background, 0, 0, 800, 450, 0, 0, 160, 90);		
		
		//UP ARROW
		if (map.getNorthLocation() != null) {
			g.drawImage(arrowUp.getScaledCopy(imageScale), 390, 20);
		}
		
		//DOWN ARROW
		if (map.getSouthLocation() != null) {
			g.drawImage(arrowDown.getScaledCopy(imageScale), 385, 400);
		}
		
		//LEFT ARROW
		if (map.getWestLocation() != null) {
			g.drawImage(arrowLeft.getScaledCopy(imageScale), 20, 200);
		}
		
		//RIGHT ARROW
		if (map.getEastLocation() != null) {
			g.drawImage(arrowRight.getScaledCopy(imageScale), 740, 200);
		}
		
		// ELEVATOR UP ARROW
		if (map.getFloor() < 3 &&
				map.getLocation().getCoordY() == 1) {
			g.drawImage(arrowUp.getScaledCopy(2), 435, 155);
		}
		if (map.getFloor() < 3 &&
				map.getLocation().getCoordY() == 2) {
			font.drawString(395, 145, String.valueOf(map.getFloor()), Color.black);
			font.drawString(397, 147, String.valueOf(map.getFloor()), Color.white);
			g.drawImage(arrowUp.getScaledCopy(4), 485, 200);
		}
		
		// ELEVATOR DOWN ARROW
		if (map.getFloor() > 1 &&
				map.getLocation().getCoordY() == 1) {
			g.drawImage(arrowDown.getScaledCopy(2), 433, 175);
		}
		if (map.getFloor() > 1 &&
				map.getLocation().getCoordY() == 2) {
			font.drawString(395, 145, String.valueOf(map.getFloor()), Color.black);
			font.drawString(397, 147, String.valueOf(map.getFloor()), Color.white);
			g.drawImage(arrowDown.getScaledCopy(4), 481, 235);
		}
		
		//BACKPACK BUTTON
		font.drawString(660, 408, BACKPACK_BUTTON, Color.black);
		font.drawString(664, 410, BACKPACK_BUTTON, Color.white);

	}

	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
	}
	
	@Override
	public void mouseReleased(int button, int x, int y) {
		if (button == 0) {
			System.out.println("X: " + x);
			System.out.println("Y: " + y);
			Map map = controller.getModel().getMap();
			
			//UP ARROW
			if ((x > 390 &&  x < 427) && (y > 22 &&  y < 58)) {
				if (map.getNorthLocation() != null) {
					map.setLocation(map.getNorthLocation());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//DOWN ARROW
			if ((x > 390 &&  x < 427) && (y > 403 &&  y < 438)) {
				if (map.getSouthLocation() != null) {
					map.setLocation(map.getSouthLocation());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//LEFT ARROW
			if ((x > 20 &&  x < 60) && (y > 203 &&  y < 238)) {
				if (map.getWestLocation() != null) {
					map.setLocation(map.getWestLocation());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//RIGHT ARROW
			if ((x > 741 &&  x < 781) && (y > 203 &&  y < 238)) {
				if (map.getEastLocation() != null) {
					map.setLocation(map.getEastLocation());	
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//ELEVATOR UP ARROW
			if ((x > 485 &&  x < 515) && (y > 203 &&  y < 231)) {
				if (map.getFloorAbove() != null) {
					map.setLocation(map.getFloorAbove());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			//ELEVATOR DOWN ARROW
			if ((x > 485 &&  x < 515) && (y > 238 &&  y < 466)) {
				if (map.getFloorBelow() != null) {
					map.setLocation(map.getFloorBelow());	
					
					// Now we have new location set for transition
					if (map.getLocation().getID() == ID) {
						Display.update();
					} else {
						game.enterState(map.getLocation().getID());
					}
				}
			}
			
			// BACKPACK BUTTON
			if ((x > 660 &&  x < 800) && (y > 419 &&  y < 441)) {
				game.enterState(BackpackMenu.ID);
			}
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

}
