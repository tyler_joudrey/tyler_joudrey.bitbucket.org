package www.Wumbo.enums;

/**
 * All possible hairstyles in the game.
 * 
 * @author: Luke Webster
 */
public enum HairStyle {
	BALD(Gender.MALE, null),
	BOB(Gender.FEMALE, "art//female-hair-3"),
	BUZZCUT(Gender.FEMALE, "art//female-hair-2"),
	LONG(Gender.FEMALE, "art//female-hair-1"),
	MULLET(Gender.MALE, "art//male-hair-2"),
	COOLIO(Gender.MALE, "art//male-hair-1"),
	MEDIUM(Gender.MALE, "art//male-hair-3");
	
	private final Gender gender;
	private final String url;
	
	HairStyle(Gender gender, String url) {
		this.gender = gender;
		this.url = url;
	}
	
	public Gender getGender() {
		return gender;
	}

	public String getUrl() {
		return url;
	}

	public HairStyle getNext() {
	     return this.ordinal() < HairStyle.values().length - 1
	         ? HairStyle.values()[this.ordinal() + 1]
	         : HairStyle.values()[0];
	}
	
	public HairStyle getPrevious() {
	     return this.ordinal() > HairStyle.values().length - HairStyle.values().length
		         ? HairStyle.values()[this.ordinal() - 1]
		         : HairStyle.values()[HairStyle.values().length - 1];
	}
}
