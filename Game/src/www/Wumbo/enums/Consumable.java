package www.Wumbo.enums;

/**
 * All food items that increase health in the game.
 * 
 * @author: Luke Webster
 */
public enum Consumable {
	COLD_PIZZA_CRUST(10),
	ENERGY_DRINK	(20),
	RAMEN_NOODLES	(40),
	BEEF_JERKY		(60),
	DAILY_SLOP		(100);
	
	private final int healthGained;
	
	Consumable(int healthGained) {
		this.healthGained = healthGained;
	}
	
	public int getHealthGained() {
		return healthGained;
	}

	@Override
	public String toString() {
		switch(this) {
		case COLD_PIZZA_CRUST:
			return "Cold Pizza Crust";
		case ENERGY_DRINK:
			return "Energy Drink";
		case RAMEN_NOODLES:
			return "Ramen Noodles";
		case BEEF_JERKY:
			return "Beef Jerky";
		case DAILY_SLOP:
			return "Daily Slop";
		default:
			throw new IllegalArgumentException();
		}
	}
}
