package www.Wumbo.enums;

/**
 * Enumeration of all outfits possible for the character.
 * 
 * @author: Luke Webster
 */
public enum Outfit {
	PINK_SHIRT(Gender.FEMALE, "art//female-outfit-1.png"),
	HOODIE(Gender.FEMALE, "art//female-outfit-2.png"),
	BLOUSE(Gender.FEMALE, "art//female-outfit-3.png"),
	POLO(Gender.MALE, "art//male-outfit-1.png"),
	EDGY(Gender.MALE, "art//male-outfit-2.png"),
	RED_SHIRT(Gender.MALE, "art//male-outfit-3.png");
	
	private final Gender gender;
	private final String url;
	
	Outfit(Gender gender, String url) {
		this.gender = gender;
		this.url = url;
	}
	
	public Gender getGender() {
		return gender;
	}

	public String getUrl() {
		return url;
	}

	public Outfit getNext() {
	     return this.ordinal() < Outfit.values().length - 1
	         ? Outfit.values()[this.ordinal() + 1]
	         : Outfit.values()[0];
	}
	
	public Outfit getPrevious() {
	     return this.ordinal() > Outfit.values().length - Outfit.values().length
		         ? Outfit.values()[this.ordinal() - 1]
		         : Outfit.values()[Outfit.values().length - 1];
	}
}
