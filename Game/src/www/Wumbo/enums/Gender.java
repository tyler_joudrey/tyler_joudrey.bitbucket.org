package www.Wumbo.enums;

/**
 * All possible genders for characters in the game.
 * 
 * @author: Luke Webster
 */
public enum Gender {
	MALE("art//Man-Base.png"),
	FEMALE("art//Woman-base.png");
	
	private final String url;
	
	Gender(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
