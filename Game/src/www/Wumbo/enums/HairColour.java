package www.Wumbo.enums;

/**
 * All possible hair colours in the game.
 * 
 * @author: Luke Webster
 */
public enum HairColour {
	BLONDE("-blonde.png"),
	BROWN(".png"),
	BLACK("-black.png"),
	WHITE("-white.png");
	
	private final String colour;
	
	private HairColour(String colour) {
		this.colour = colour;
	}

	public String getColour() {
		return colour;
	}

	public HairColour getNext() {
	     return this.ordinal() < HairColour.values().length - 1
	         ? HairColour.values()[this.ordinal() + 1]
	         : HairColour.values()[0];
	}
	
	public HairColour getPrevious() {
	     return this.ordinal() > HairColour.values().length - HairColour.values().length
		         ? HairColour.values()[this.ordinal() - 1]
		         : HairColour.values()[HairColour.values().length - 1];
	}
}
